import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { BackEndService } from './backend.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Model driven form test!';

  form: FormGroup;
    
  productName = new FormControl("<enter product name>", Validators.required);
  productManufacturer = new FormControl("<enter manufacturer>", Validators.required); 
  productPrice = new FormControl("<enter price>", Validators.required);
    
  constructor(fb: FormBuilder, private _backEndService: BackEndService) {
     this.form = fb.group({
        "productName": this.productName,
        "productManufacturer": this.productManufacturer,
        "productPrice": this.productPrice
     });
  }
  onSubmitModelBased() {
     console.log('Form: '+this.form);
     console.log('Product name: '+this.form.value.productName);
     console.log('Product manufacturer: '+this.form.value.productManufacturer);
     console.log('Product price: '+this.form.value.productPrice);

     let self = this;
     this._backEndService.invokeBackend(this.form.value).subscribe(response => {
       alert('Backend sent back: '+response.text());
       self.form.reset();
     });
  }
}
