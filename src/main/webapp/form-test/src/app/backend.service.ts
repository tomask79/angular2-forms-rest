import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class BackEndService {
    constructor (public _http: Http) {
    }

    public invokeBackend(payload: string) : Observable<Response>{
        let body = JSON.stringify(payload);
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        console.log('Going to invoke post with payload: '+body);
        return this._http.post
            ('http://localhost:8080/saveProduct', body, options);
    }
}