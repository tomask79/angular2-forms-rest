import { Angular2FormsRevisitedPage } from './app.po';

describe('angular2-forms-revisited App', function() {
  let page: Angular2FormsRevisitedPage;

  beforeEach(() => {
    page = new Angular2FormsRevisitedPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
