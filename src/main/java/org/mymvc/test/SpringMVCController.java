package org.mymvc.test;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PostMapping;

@RestController
public class SpringMVCController {
	
	@PostMapping(value = "/saveProduct")
	public String saveProduct(@RequestBody final Product product) {
		return "Spring received product: \n"
						+ "Name: "+product.getProductName()+" \n"
						+" Price "+product.getProductPrice()+" \n"
						+" Manufacturer: "+product.getProductManufacturer();
	}
}
