# Writing applications in Angular 2 [part 22] #

## Angular 2 Forms and Spring Rest ##

In part 22 let's test [Angular 2](https://angular.io/) interaction with [Spring REST](https://projects.spring.io/spring-restdocs/). 
For this we're going to need to revise [Angular2 forms](https://angular.io/docs/ts/latest/guide/forms.html) in the final version which is out since [14.09.2016](https://github.com/angular/angular/blob/master/CHANGELOG.md). 

## Angular 2 Forms ##

Well even if a lot of things has changed since I did [part 7](https://bitbucket.org/tomask79/angular2-forms-intro), [part 8](https://bitbucket.org/tomask79/angular2-forms-formbuilder) and [part 9](https://bitbucket.org/tomask79/angular2-forms-customvalidations) you still have two options when creating forms in Angular 2, your form can be:

* Template driven
* Model Driven

I won't be retyping documentation, just simply read following excellent article,[Introduction to Angular 2 Forms](http://blog.angular-university.io/introduction-to-angular-2-forms-template-driven-vs-model-driven/) Let's just say that in case of **Template driven** form, most of the infrastructure like FormGroup, controls and validations you configure inside of template and on the other hand in **Model Driven** form you configure form stuff in the component, which is more powerful, flexible and most of all **testable**.

## Angular2 Form Posting data to Spring REST ##

Let's create a Model Driven Form with three controls. ProductName, productManufacturer and productPrice. 

**app.component.html:**

```
<h1>
  {{title}}
</h1>
<form [formGroup]="form" (ngSubmit)="onSubmitModelBased()">
      <p>
          <label>Product name:</label>
          <input type="text" formControlName="productName">
      </p>
      <p>
          <label>Product manufacturer:</label>
          <input type="text" formControlName="productManufacturer">
      </p>
      <p>
          <label>Product price:</label>
          <input type="text" formControlName="productPrice">
      </p>
      <p>
          <button type="submit" [disabled]="!form.valid">Submit</button>
      </p>
</form>
```
app.component.ts:

```
import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { BackEndService } from './backend.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Model driven form test!';

  form: FormGroup;
    
  productName = new FormControl("<enter product name>", Validators.required);
  productManufacturer = new FormControl("<enter manufacturer>", Validators.required); 
  productPrice = new FormControl("<enter price>", Validators.required);
    
  constructor(fb: FormBuilder, private _backEndService: BackEndService) {
     this.form = fb.group({
        "productName": this.productName,
        "productManufacturer": this.productManufacturer,
        "productPrice": this.productPrice
     });
  }
  onSubmitModelBased() {
     console.log('Form: '+this.form);
     console.log('Product name: '+this.form.value.productName);
     console.log('Product manufacturer: '+this.form.value.productManufacturer);
     console.log('Product price: '+this.form.value.productPrice);

     let self = this;
     this._backEndService.invokeBackend(this.form.value).subscribe(response => {
       alert('Backend sent back: '+response.text());
       self.form.reset();
     });
  }
}
```
* With FormBuilder we created **FormGroup** holding state and value of the form.
* We added three product **FormControls** into that FormGroup.
* FormControl can be set during adding into FormGroup or separately like I did in the example, I always prefer second option. 
* We injected BackEndService which POSTs data to the Spring REST and returns Observable to which app.component subscribes to get the [Response](https://angular.io/docs/ts/latest/api/http/index/Response-class.html) from the server.

BackEndService.ts

```
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class BackEndService {
    constructor (public _http: Http) {
    }

    public invokeBackend(payload: string) : Observable<Response>{
        let body = JSON.stringify(payload);
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        console.log('Going to invoke post with payload: '+body);
        return this._http.post
            ('http://localhost:8080/saveProduct', body, options);
    }
}
```

## Spring REST POST method Controller ##

Now let's build Spring REST controller for reading the posted data. From Angular 2 we're sending Product parameters so first, let's create POJO which [Jackson](https://github.com/FasterXML/jackson) will use to convert JSON sent from Angular 2 form.

```
package org.mymvc.test;

public class Product {
	private String productName;
	private String productManufacturer;
	private String productPrice;

	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getProductManufacturer() {
		return productManufacturer;
	}
	public void setProductManufacturer(String productManufacturer) {
		this.productManufacturer = productManufacturer;
	}
	public String getProductPrice() {
		return productPrice;
	}
	public void setProductPrice(String productPrice) {
		this.productPrice = productPrice;
	}
}
```

Now Controller:

```
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PostMapping;

@RestController
public class SpringMVCController {
	
	@PostMapping(value = "/saveProduct")
	public String saveProduct(@RequestBody final Product product) {
		return "Spring received product: \n"
						+ "Name: "+product.getProductName()+" \n"
						+" Price "+product.getProductPrice()+" \n"
						+" Manufacturer: "+product.getProductManufacturer();
	}
}
```
* Did you know that since Spring 4.3 we can use [PostMapping](http://docs.spring.io/spring/docs/current/javadoc-api/org/springframework/web/bind/annotation/PostMapping.html) annotation which is a shortcut of RequestMapping annotation with method = POST parameter? I didn't..:-)

* Returned Observable to component app.component is an **Cold Observable**. Don't be surprised that the request is not sent if you don't subscribe to the returned Observable. See this StackOverflow: http://stackoverflow.com/questions/38043247/angular-2-http-post-request-is-not-being-called-out

# Testing the demo # 

* mvn clean install (in the root directory with pom.xml)
* mvn tomcat7:run-war-only
* hit the URL http://localhost:8080 and try to post something to Spring.

regards

Tomas

